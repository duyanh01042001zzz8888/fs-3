from locust import HttpUser, task


class Post(HttpUser):

    @task
    def get_all(self):
        self.client.get("/posts?page=1&limit=10")

    @task
    def update(self):
        headers = {
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiZXhwIjoxNjYyMDE4MTQ4LCJ1c2VyIjoiRHV5IEFuaCJ9.hGoc3WpKmtcCS2iVpqkmq2NZQQ6Fbd71-ZQxFCtAmHo",
        }
        data = [
            {
                "type": "text",
                "value": "zzzz"
            }
        ]
        self.client.put(url="/post/4459465", headers=headers, data=data)
