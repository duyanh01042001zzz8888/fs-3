package main

import (
	"crypto/tls"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/myzhan/boomer"
)

var client *http.Client
var postBody []byte

var verbose bool

var method string
var _url string
var timeout int
var postFile string
var contentType string

var disableCompression bool
var disableKeepalive bool

func worker() {

	data := url.Values{}
	data.Set("url", "https://vnexpress.net/du-kien-bo-phan-hang-dao-duc-giao-vien-4465867.html")
	// data.Set("password", "123")

	request, err := http.NewRequest(method, _url, strings.NewReader(data.Encode()))
	// request, err := http.NewRequest(method, _url, bytes.NewBuffer(postBody))
	if err != nil {
		log.Fatalf("%v\n", err)
	}

	request.Header.Set("Content-Type", contentType)
	request.Header.Set("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiZXhwIjoxNjYyNjIxNDQyLCJ1c2VyIjoiRHV5IEFuaCJ9.SYaDJ__0oJaUT4kVcu5d7GsdpUDfR77wcWWIzBGDwk4")

	startTime := time.Now()
	response, err := client.Do(request)
	elapsed := time.Since(startTime)

	if err != nil {
		if verbose {
			log.Printf("%v\n", err)
		}
		boomer.RecordFailure("http", method+" "+_url, 0.0, err.Error())
	} else {
		// body, _ := ioutil.ReadAll(response.Body)
		// fmt.Println(string(body))

		boomer.RecordSuccess("http", method+" "+_url,
			elapsed.Nanoseconds()/int64(time.Millisecond), response.ContentLength)

		if verbose {
			body, err := ioutil.ReadAll(response.Body)
			if err != nil {
				log.Printf("%v\n", err)
			} else {
				log.Printf("Status Code: %d\n", response.StatusCode)
				log.Println(string(body))
			}

		} else {
			io.Copy(ioutil.Discard, response.Body)
		}

		response.Body.Close()
	}
}

func main() {
	flag.StringVar(&method, "method", "GET", "HTTP method, one of GET, POST")
	flag.StringVar(&_url, "url", "", "URL")
	flag.IntVar(&timeout, "timeout", 10, "Seconds to max. wait for each response")
	flag.StringVar(&postFile, "data", "", "File containing data to POST. Remember also to set --content-type")
	flag.StringVar(&contentType, "content-type", "text/plain", "Content-type header")

	flag.BoolVar(&disableCompression, "disable-compression", false, "Disable compression")
	flag.BoolVar(&disableKeepalive, "disable-keepalive", false, "Disable keepalive")

	flag.BoolVar(&verbose, "verbose", false, "Print debug log")

	flag.Parse()

	log.Printf(`HTTP benchmark is running with these args:
method: %s
url: %s
timeout: %d
data: %s
content-type: %s
disable-compression: %t
disable-keepalive: %t
verbose: %t`, method, _url, timeout, postFile, contentType, disableCompression, disableKeepalive, verbose)

	if _url == "" {
		log.Fatalln("--url can't be empty string, please specify a URL that you want to test.")
	}

	if postFile != "" {
		tmp, err := ioutil.ReadFile(postFile)
		if err != nil {
			log.Fatalf("%v\n", err)
		}
		postBody = tmp
	}

	http.DefaultTransport.(*http.Transport).MaxIdleConnsPerHost = 2000
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
		MaxIdleConnsPerHost: 2000,
		DisableCompression:  disableCompression,
		DisableKeepAlives:   disableKeepalive,
	}
	client = &http.Client{
		Transport: tr,
		Timeout:   time.Duration(timeout) * time.Second,
	}

	task := &boomer.Task{
		Name:   "worker",
		Weight: 10,
		Fn:     worker,
	}

	boomer.Run(task)
}
